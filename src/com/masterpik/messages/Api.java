package com.masterpik.messages;

import com.masterpik.api.util.UtilString;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class Api {

    public static String getString(String path, boolean isSpigot) {
        return getString(path, isSpigot, true);
    }

    public static String getString(String path, boolean isSpigot, boolean showErrors) {
        if (isSpigot) {
            return com.masterpik.messages.spigot.Api.getString(path, showErrors);
        } else {
            return com.masterpik.messages.bungee.Api.getString(path, showErrors);
        }
    }

    public static void setString(String path, String string, boolean isSpigot) {
        if (isSpigot) {
            com.masterpik.messages.spigot.Api.setString(path, string);
        }
    }

}
