package com.masterpik.messages.spigot;

import com.masterpik.api.util.UtilString;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class Api {

    public static String getString(String path) {
        return getString(path, true);
    }

    public static String getString(String path, boolean showErrors) {
        String rt = "";

        String pathTr = ""+path;
        //pathTr = pathTr.replace('.', ':');
        String[] pathA = UtilString.PointStringPathToArray(pathTr);

        //Bukkit.getLogger().info("path:"+path+" | path tr:"+pathTr);

        File file = new File("MasterpikMessages/"+pathA[0]+".yml");

        /*Bukkit.getLogger().info("absolute path: "+file.getAbsolutePath());
        try {
            Bukkit.getLogger().info("canonical path: "+file.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        if (file.exists()) {

            FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

            if (fileConfig.contains(path)) {

                rt = fileConfig.getString(path);

                //Bukkit.getLogger().info("paht : " + path + " ||||||| return : " + rt);

                return rt;
            }
            else {

                if (showErrors) {
                    Bukkit.getLogger().info("[MasterpikMessages] Api.getString error, the path " + path + " is empty");
                }

                return null;
            }
        }
        else {

            if (showErrors) {
                Bukkit.getLogger().info("[MasterpikMessages] Api.getString error, the file " + pathA[0] + ".yml doesn't exist");
            }

            return null;
        }
    }

    public static void setString(String path, String string) {
        String[] pathA = UtilString.PointStringPathToArray(path);

        File file = new File("MasterpikMessages/"+pathA[0]+".yml");
        FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

        fileConfig.set(path, string);

        try {fileConfig.save(file);} catch (IOException e1) {e1.printStackTrace();}
    }

}
