package com.masterpik.messages.bungee;

import com.masterpik.api.util.UtilString;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class Api {
    public static String getString(String path) {
        return getString(path, true);
    }

    public static String getString(String path, boolean showErrors) {
        String rt = "";

        String pathTr = ""+path;
        //pathTr = pathTr.replace('.', ':');
        String[] pathA = UtilString.PointStringPathToArray(pathTr);

        //Bukkit.getLogger().info("path:"+path+" | path tr:"+pathTr);

        File file = new File("MasterpikMessages/"+pathA[0]+".yml");

        /*Bukkit.getLogger().info("absolute path: "+file.getAbsolutePath());
        try {
            Bukkit.getLogger().info("canonical path: "+file.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        if (file.exists()) {

            try {
                Configuration fileConfig = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);

                if (fileConfig.get(path) != null) {

                    rt = fileConfig.getString(path);

                    //Bukkit.getLogger().info("paht : " + path + " ||||||| return : " + rt);

                    return rt;
                }
                else {

                    if (showErrors) {
                        ProxyServer.getInstance().getLogger().info("[MasterpikMessages] Api.getString error, the path " + path + " is empty");
                    }

                    return null;
                }


            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

        }
        else {

            if (showErrors) {
                ProxyServer.getInstance().getLogger().info("[MasterpikMessages] Api.getString error, the file " + pathA[0] + ".yml doesn't exist");
            }

            return null;
        }
    }
}
